const http = require('http');

const PORT = 3000;

http.createServer(function(request,response){

	if(request.url == '/login'){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('This is the login page');
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Error: Page not found');
	}

}).listen(PORT)

console.log(`Server is running at localhost: ${PORT}.`)